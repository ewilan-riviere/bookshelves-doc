---
title: Entities & relationships
position: 5
category: Back
---

## Book

Main entity, a Book have many fields with a lot of relationships, here an example with `Vingt ans après` from `Alexandre Dumas` in `Les Trois Mousquetaires`. Some of fields are from OPF file (which is into EPUB file), if you want to change these values you will have to update EPUB file.

>The badge <badge>**extracted**</badge> indicate information have exist into scanned file.
>The badge <badge>**OPF**</badge> show the information is from OPF.  
>The badge <badge>**OPF Calibre**</badge> show the information is from OPF updated with Calibre.

- `title`: `string` title of book, example: `Vingt ans après` <badge>**extracted**</badge> <badge>**required**</badge>
- `slug`: `string` slug of book's `title` with `language` code, example: `vingt-ans-apres-fr` <badge>**generated**</badge>
  >`language` code associated with book is useful to avoid override books, API routes offer `show` routes with this syntax: `/books/{author_slug}/{book_slug}`. If the author write a book with a specific title, this book can have a translation with same title: *Dune* of Frank Herbert, the volume 1 have title *Dune* in english (`dune-en`) and in french (`dune-fr`).
- `slug_sort`: `string` *formated[^1] slug title of the book* with *formated[^1] slug title of series* (if book have a series) with volume, example: `trois-mousquetaires-02_vingt-ans-apres-fr` <badge>**generated**</badge>
  >to sort with *natural order* a list of books, it's possible to list by title but all books of same series can begin with a specific title, to avoid a heavy process of `groupBy`, Bookshelves offer a generated sorting title to keep all books of same series together based on series' title.
- `contributor`: `string` EPUB contributions. <badge>**extracted**</badge>
- `description`: `text` abstract of book, can be a long text with HTML. <badge>**extracted**</badge>
- `released_on`: `datetime` release date when Book have been published. <badge>**extracted**</badge>
- `rights`: `string` EPUB copyrights. <badge>**extracted**</badge>
- `volume`: `integer` volume number into a series. <badge>**extracted**</badge>
- `page_count`: `integer` <badge>**API**</badge>
- `maturity_rating`: `string` <badge>**API**</badge>
- `disabled`: `boolean` to hide a Book from API
- `type`: `enum` can be `comic`, `essay`, `handbook` or `novel`
- `isbn10`: `string` <badge>**extracted**</badge> <badge>**API**</badge>
- `isbn13`: `string` <badge>**extracted**</badge> <badge>**API**</badge>
- `identifiers`: `json` <badge>**extracted**</badge>
- **medias**:
  - `cover`

### Relations

- `authors` <badge>**extracted**</badge>
- `language` <badge>**extracted**</badge>
- `serie` <badge>**extracted**</badge>
- `publisher` <badge>**extracted**</badge>
- `tags` <badge>**extracted**</badge>

[^1] : **formated**: when slug will be generated all determiner words will be removed, to sort by relevant words. Example: `La fureur du fleuve` from `Les Cités des Anciens`, vol. `3`, series from `Robin Hobb` will become `cites-des-anciens-03_fureur-du-fleuve`
