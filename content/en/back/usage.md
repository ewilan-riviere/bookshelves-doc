---
title: Usage
position: 3
category: Back
---

## **I. Store eBooks**

You have to check if `storage:link` is effective with

```bash
php artisan storage:link
```

**On Bookshelves you have to store your EPUB files into one directory: `public/storage/data/books`**

If you want to know why, it's simple, EPUB files aren't on git of course, but more it's not really practical to store all ebooks into Bookshelves directly, it's more convenient to set symbolic link from your eBooks storage which is not into Bookshelves. But you can store EPUB files into `public/storage/data/books` directory, of course, Bookshelves scan recursively this directory, you can have sub directories if you want.  

### *Option 1: store directly EPUB files*

Git won't track any file into `public/storage/data/books`

```bash
.
+-- public
|   +-- storage
|     +-- raw
|       +-- books
|         +-- my-ebook.epub
|         +-- ...
```

### *Option 2: create symbolic link*

This is best solution for my usage, I have some directories with different eBooks but I want to refer all these directories.

```bash
ln -s /any/directory/books-fantasy public/storage/data/books
ln -s /any/directory/books-classic public/storage/data/books
```

```bash
.
+-- public
|   +-- storage
|     +-- raw
|       +-- books
|         +-- books-fantasy -> /any/directory/books-fantasy
|         +-- books-classic -> /any/directory/books-classic
```

#### With variant: EPUB -> link

It's possible to link all EPUB files too but if you have any update from original directories, it can broke links or you have to link again.

```bash
ln -s /any/directory/books-fantasy/**/*.epub public/storage/data/books 
ln -s /any/directory/books-classic/**/*.epub public/storage/data/books
```

```bash
.
+-- public
|   +-- storage
|     +-- raw
|       +-- books
|         +-- my-ebook.epub -> /any/directory/books-fantasy/my-ebook.epub
|         +-- another-ebook.epub -> /any/directory/books-classic/another-ebook.epub
```

## **II. Custom data**

When Bookshelves parse EPUB to generate data, it will try to find extra data from Internet on GoogleBooks and Wikipedia. If you don't want to get these data, you can use `--local` option to skip it. But if you want to set custom data directly from parsing by Bookshelves, it's possible.

### *a. Description and link*

For extra data with authors and series for `description` and `link`, you can create JSON file. If an entry with `slug` of author or serie exist, Bookshelves will take it and don't use external API.

```bash
cp public/storage/data/authors.example.json public/storage/data/authors.json
cp public/storage/data/series.example.json public/storage/data/series.json
```

An example for authors.

```json
{
    "hugo-victor": {
        "description": "Victor Hugo est un poète, dramaturge, écrivain, romancier et dessinateur romantique français, né le 7 ventôse an X (26 février 1802) à Besançon et mort le 22 mai 1885 à Paris. Il est considéré comme l'un des plus importants écrivains de la langue française. Il est aussi une personnalité politique et un intellectuel engagé qui a eu un rôle idéologique majeur et occupe une place marquante dans l'histoire des lettres françaises au XIXe siècle.",
        "link": "https://gallica.bnf.fr"
    }
}
```

### *b. Picture*

Bookshelves will use cover of first book of a serie to generate cover's serie and will use Wikipedia API to get picture of an author. You can set custom pictures for series and authors. Just put **JPG** file with `slug` of author / serie into specific directory.

- `public/storage/data/pictures-authors`: for authors
- `public/storage/data/pictures-series`: for series

```bash
.
+-- public
|   +-- storage
|     +-- raw
|       +-- pictures-authors
|         +-- hugo-victor.jpg
|         +-- ...
```

You can set symbolic link like this to get pictures from another directory.

```bash
ln -s /any/directory/authors public/storage/data/pictures-authors
ln -s /any/directory/series public/storage/data/pictures-series
```

```bash
.
+-- public
|   +-- storage
|     +-- raw
|       +-- pictures-authors
|         +-- authors -> /any/directory/authors
```

## **VI. Features**

### *g. MetadataExtractor*

TODO

### *i. Wikipedia*

TODO

- WikipediaService
