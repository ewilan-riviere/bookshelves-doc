---
title: NGINX
category: Deployment
position: 1
---

This application works with **Server Side Rendering**, so you need to serve back-end with virtual host and front-end with a service like [**PM2**](https://pm2.keymetrics.io/).

To know requirements before deployment, [**check technologies**](/development/requirement#technologies)

## Configuration

Here you have an example for NGINX, note the `proxy_params` on `3001` port (can be any other port) is for PM2.

```nginx
server {
  listen 80;
  server_name bookshelves.ink;
  root /path/to/repository/bookshelves-back/public;
  index index.php index.html;

  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-XSS-Protection "1; mode=block";
  add_header X-Content-Type-Options "nosniff";
  add_header X-Robots-Tag "index, follow";

  charset utf-8;

  error_log /var/log/nginx/bookshelves.log warn;
  access_log /var/log/nginx/bookshelves.log;

  location / {
    include proxy_params;
    proxy_pass http://localhost:3001;
  }

  location ~ ^/(admin|api|assets|catalog|docs|opds|storage|webreader) {
    proxy_pass http://localhost:3002;
  }

  location ~ \.php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
  }
}
```

## PM2

You can install PM2 with NPM.

```bash
npm install pm2@latest -g
```

Here a config for `ecosystem.config.js`, you can create this config at `~/` for example. Take same port as `proxy_params`.

```js[~/ecosystem.config.js]
module.exports = {
  apps: [
    {
      name: "bookshelves",
      script: "npm",
      cwd: "/path/to/repository/bookshelves-front",
      args: "start",
      env: {
        PORT: 3001,
      },
    },
  ],
};
```

Start PM2 with this config and save it.

```bash
pm2 start ~/ecosystem.config.js
pm2 save
```

## Build back-end

You have to build back-end to serve back-office, Catalog and webreader. You will have to get `node_modules` and generate assets with `pnpm build`.

```bash
pnpm i
php artisan view:clear
php artisan config:clear
pnpm build
```

### From root

These directories or endpoints will be served from `/`.

#### Directories

- `assets`
- `docs`
- `storage`

#### Endpoints

- `admin`
- `api`
- `catalog`
- `opds`
- `webreader`
