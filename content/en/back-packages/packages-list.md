---
title: List
position: 1
category: Back - Packages
---

## PHP dependencies with `composer`

`composer.json` list some dependencies, you have to install them with [**Composer v2**](https://getcomposer.org/).

### Depedencies

- artesaos/seotools: <https://github.com/artesaos/seotools>
- fluentdom/fluentdom
- fruitcake/laravel-cors
- genl/matice
- guzzlehttp/guzzle
- http-interop/http-factory-guzzle
- inertiajs/inertia-laravel: <https://github.com/inertiajs/inertia-laravel>
- itsgoingd/clockwork: <https://github.com/itsgoingd/clockwork>
- laravel/fortify: <https://laravel.com/docs/9.x/fortify>
- laravel/framework
- laravel/sanctum: <https://laravel.com/docs/9.x/sanctum>
- laravel/scout: <https://laravel.com/docs/9.x/scout>
- laravel/tinker
- league/commonmark
- league/glide-symfony: <https://github.com/thephpleague/glide-symfony>
- league/html-to-markdown: <https://github.com/thephpleague/html-to-markdown>
- livewire/livewire
- maatwebsite/excel: <https://github.com/SpartnerNL/Laravel-Excel>
- meilisearch/meilisearch-php
- mobiledetect/mobiledetectlib
- oscarotero/inline-svg
- spatie/array-to-xml: <https://github.com/spatie/array-to-xml>
- spatie/eloquent-sortable: <https://github.com/spatie/eloquent-sortable>
- spatie/laravel-enum: <https://github.com/spatie/laravel-enum>
- spatie/laravel-image-optimizer: <https://github.com/spatie/laravel-image-optimizer>
- spatie/laravel-medialibrary: <https://spatie.be/docs/laravel-medialibrary/v10/introduction>
- spatie/laravel-query-builder: <https://github.com/spatie/laravel-query-builder>
- spatie/laravel-route-attributes: <https://github.com/spatie/laravel-route-attributes>
- spatie/laravel-sluggable: <https://github.com/spatie/laravel-sluggable>
- spatie/laravel-tags: <https://github.com/spatie/laravel-tags>
- spatie/laravel-translatable: <https://github.com/spatie/laravel-translatable>
- tightenco/ziggy

### Dev dependencies

- barryvdh/laravel-ide-helper: <https://github.com/barryvdh/laravel-ide-helper>
- beyondcode/laravel-dump-server: <https://github.com/beyondcode/laravel-dump-server>
- fakerphp/faker
- friendsofphp/php-cs-fixer
- knuckleswtf/scribe: <https://github.com/knuckleswtf/scribe>
- laravel/sail: <https://laravel.com/docs/9.x/sail>
- mockery/mockery
- nunomaduro/collision
- nunomaduro/larastan
- pestphp/pest-plugin-laravel
- phpro/grumphp-shim
- phpunit/phpunit
- spatie/laravel-ignition: <https://github.com/spatie/laravel-ignition>
