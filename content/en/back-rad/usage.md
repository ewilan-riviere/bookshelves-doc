---
title: Usage
position: 2
category: Back - RAD Stack
---

## Create new entity

To create a new entity into RAD Stack, you have to create some files, in this example replace `model` with name of your Model

- `ModelExport.php` in `app/Exports`
- `ModelController.php` in `app/Http/Controllers/Admin`
- `ModelQuery.php` in `app/Http/Queries`
- `ModelResource.php` in `app/Http/Resources/Admin`
- `models/Index.vue` in `resources/admin/pages`
  - `models/Create.vue` in `resources/admin/pages` *optional*
  - `models/Edit.vue` in `resources/admin/pages` *optional*
  - `ModelForm.vue` in `resources/admin/components/forms` *optional*
- `model.ts` in `resources/admin/types` and export from `resources/admin/types/index.ts`
- Add new model to `resources/lang/en/crud.php`
  - Add new model to `resources/lang/fr/crud.php` for french
  - Add new attributes intro `attributes` key in `resources/lang/en/admin.php` for global attributes
- Add new entry to `helpers.ts` in `resources/admin/features`
- Create new routes in `routes/web.php`
- Create new entry in `resources/admin/_nav.ts`

## Enums

To add an Enum available, you have to create a new Enum like `app/Enums/BookTypeEnum.php`

```php[app/Enums/BookTypeEnum.php]
<?php

namespace App\Enums;

/**
 * @method static self handbook()
 * @method static self essay()
 * @method static self comic()
 * @method static self novel()
 */
final class BookTypeEnum extends Enum
{
  protected static function labels(): array
  {
    return [
      'handbook' => 'Handbook',
      'essay' => 'Essay',
      'comic' => 'Comic',
      'novel' => 'Novel',
    ];
  }
}
```

### Add to Model *optional*

If you want to add Enum to your Model, add it with `casts`

```php[app/Models/Book.php]
<?php

namespace App\Models;

use App\Enums\BookTypeEnum;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
  protected $fillable = [
    'type',
  ];
  protected $casts = [
    'type' => BookTypeEnum::class.':nullable'
  ];
}
```

### Add Enum to Inertia

You have to add Enum to middleware `HandleInertiaRequests.php`

```php[app/Http/Middleware/HandleInertiaRequests.php]
<?php

namespace App\Http\Middleware;

use App\Enums\BookTypeEnum;
use Illuminate\Http\Request;
use Inertia\Middleware;

class HandleInertiaRequests extends Middleware
{
  public function share(Request $request)
  {
    return array_merge(parent::share($request), [
      'enums' => function () {
        return collect([
          'book_types' => BookTypeEnum::class,
        ])
          ->mapWithKeys(fn ($enum, $key) => [$key => $enum::toArray()])
        ;
      },
    ]);
  }
}
```

Add Enum to `EnumTypes` to get autocompletion and `enumVariants` if you want to get custom colors.

```ts[resources/admin/types/enums.ts]
export interface EnumTypes {
  book_types: { [key: string]: string }
}

export const enumVariants = {
  book_types: {
    handbook: 'bg-gray-100',
    essay: 'bg-blue-100',
    comic: 'bg-green-100',
    novel: 'bg-primary-100',
  },
}
```

In list of models, you can use new Enum

```vue[resources/admin/pages/books/Index.vue]
<script lang="ts" setup>
  import { Column } from '@admin/types/data-table'

  const columns: (string | Column)[] = [
    {
      field: 'type',
      type: 'select',
      props: {
        choices: 'book_types',
      },
      searchable: true,
    },
  ]
</script>
```
